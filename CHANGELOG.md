## [6.8.2] 2024-08-12
### Library
- Packaged with `lib 6.8.57`

## [6.8.1] 2022-07-06
### Fixed
- Wrong ad url detection
### Library
- Packaged with `lib 6.8.24`

## [6.8.0] 2021-09-03
### Fixed
- Fake buffer reported when pausing/resuming
### Library
- Packaged with `lib 6.8.2`

## [6.7.9] 2021-08-09
### Fixed
- Bug with drm detection when no drm was used
### Library
- Packaged with `lib 6.7.42`

## [6.7.8] 2021-07-13
### Added
- Drm automatic detection when only one is available
- Closing ad break when ad timeout happens
### Library
- Packaged with `lib 6.7.40`

## [6.7.7] 2021-07-06
### Added
- Buffer listener
- Playrate getter fallback checking video tag
- Jointime detection for live improvement

## [6.7.6] 2021-06-02
### Added
- Improved fullscreen detection
### Removed
- Ad errors when loading wrongly dinamically ad breaks after the actual break
### Fixed
- Ad position detection
### Library
- Packaged with `lib 6.7.35`

## [6.7.5] 2020-10-13
### Library
- Packaged with `lib 6.7.17`

## [6.7.4] 2020-07-21
### Added
- Listener to `remove` event to close the view
### Library
- Packaged with `lib 6.7.12`

## [6.7.3] 2020-07-01
### Fixed
- Wrong bitrate (higher than actual one) reported when manually changing video quality
### Library
- Packaged with `lib 6.7.11`

## [6.7.2] 2020-05-27
### Library
- Packaged with `lib 6.7.7`

## [6.7.1] 2020-05-13
### Updated
- Ad detection in some cases
### Fixed
- Ad errors now are fatal
### Library
- Packaged with `lib 6.7.6`

## [6.7.0] 2020-03-25
### Updated
- Changed bitrate reporting for some cases
### Library
- Packaged with `lib 6.7.0`

## [6.5.7] 2019-11-29
### Fixed
- Wrong behaviour with pause and seek being mixed

## [6.5.6] 2019-11-06
### Fixed
- Wrong bitrate values reported before jointime

## [6.5.5] 2019-11-05
### Library
- Packaged with `lib 6.5.19`

## [6.5.4] 2019-10-17
### Fixed
 - Additional way to detect live content
 - Improved way to get playhead for negative values
 - Jointime not sent for live content
 - Monitor disabled for negative static value for some live content
 - Stop being reported before errors because of changeResource event
### Library
- Packaged with `lib 6.5.17`

## [6.5.3] 2019-08-22
### Library
- Packaged with `lib 6.5.13`

## [6.5.2] 2019-08-19
### Library
- Packaged with `lib 6.5.12`

## [6.5.1] 2019-08-12
### Library
- Packaged with `lib 6.5.11`

## [6.5.0] 2019-05-31
### Library
- Packaged with `lib 6.5.2`

## [6.4.10]
### Library
- Packaged with `lib 6.4.23`

## [6.4.9]
### Fixed
 - Ad adapter causing crashes when trying to unregister listeners without player reference
### Library
- Packaged with `lib 6.4.22`

## [6.4.8]
### Fixed
 - Blocked start using timeupdate if the player crashed

## [6.4.7]
### Fixed
 - Seek event never sent on windows devices
### Added
 - Fatal errors by error code

## [6.4.6]
### Fixed
 - Wrong ad position received check: not `pre`, `mid` nor `post`
### Library
- Packaged with `lib 6.4.15`

## [6.4.5]
### Library
- Packaged with `lib 6.4.13`

## [6.4.4]
### Fixed
- Check for no error message

## [6.4.3]
### Added
- Added error codes
### Library
- Packaged with `lib 6.4.9`

## [6.4.2]
### Added
- Added error codes
- Ignored retry error

## [6.4.1]
### Library
- Packaged with `lib 6.4.8`

## [6.4.0]
### Added
- Fatal error detection case (keyword: down)
### Library
- Packaged with `lib 6.4.7`

## [6.3.2]
### Added
- Fatal error detection case
### Library
- Packaged with `lib 6.3.5`

## [6.3.1]
### Fixed
- Options setter for v6 format options
### Added
- Added support to set all the options using playlist options

## [6.3.0]
### Library
 - Lib updated to 6.3.0
### Fixed
 - Case with no video quality
 - False buffer underruns with 8.2+ player versions
 - Playrate 1 when paused

## [6.2.1]
### Fixed
 - Fixed ad position detection when adposition object doesnt exist

## [6.2.0]
### Fixed
 - Removed ad url because is not reliable
### Library
- Packaged with `lib 6.2.0`

## [6.1.3]
### Fixed
- Improved getBitrate and getRendition to prevent console warnings
- Added stop to some error cases
### Library
- Packaged with `lib 6.1.14`

## [6.1.2]
### Fixed
- Fixed stop after ad error for jwplayer 8.1
- Fixed onclick ad url
- Seeking will report only one seek
### Added
- Added getter for playrate
- Added underrun for ads
- Added 'auto' rendition for that case
### Library
- Packaged with `lib 6.1.11`

## [6.1.1]
### Library
- Packaged with `lib 6.1.9` fixing P2P params

## [6.1.0]
### Library
- Packaged with `lib 6.1.8`

## [6.0.0]
### Library
- Packaged with `lib 6.0.8`
