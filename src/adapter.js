var youbora = require('youboralib')
var manifest = require('../manifest.json')
var Util = youbora.Util

youbora.adapters.JWPlayer = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    var ret = this.player.getPosition()
    if (ret && ret < 0) {
      ret = Math.abs(ret)
      if (this.monitor) this.monitor.stop()
    }
    return ret
  },

  getPlayrate: function () {
    var ret = 1
    if(this.tag && this.tag.playbackRate) {
      ret = this.tag.playbackRate
    } else if (this.player.getPlaybackRate) {
      ret = this.player.getPlaybackRate()
    }
    return ret
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.duration || this.player.getDuration() || null
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    var ret = -1
    var quality = this.player.getVisualQuality()
    if (typeof this.player.getCurrentQuality() === 'number' && this.player.getQualityLevels() &&
        this.player.getCurrentQuality() !== -1) {
      quality = this.player.getQualityLevels()[this.player.getCurrentQuality()]
      if (quality && quality.bitrate) {
        ret = quality.bitrate
      }
    }
    quality = this.player.getVisualQuality()
    if (ret <= 0 && quality && quality.bitrate) {
      ret = quality.bitrate
    } else if (ret <= 0 && quality && quality.level && quality.level.bitrate) {
      ret = quality.level.bitrate
    }
    return ret
  },

  /** Override to return rendition */
  getRendition: function () {
    var ret = null
    var quality = this.player.getVisualQuality()
    if (quality && quality !== null && quality.level) {
      ret = Util.buildRenditionString(
        quality.level.width,
        quality.level.height,
        quality.level.bitrate || 0
      )
    } else if (typeof this.player.getCurrentQuality() === 'number' && this.player.getQualityLevels() && this.player.getCurrentQuality() !== -1) {
      quality = this.player.getQualityLevels()[this.player.getCurrentQuality()]
      if (quality && quality.label === 'auto') {
        ret = Util.buildRenditionString(quality.bandwidth)
      } else if (quality && quality.label) {
        ret = quality.label
      } else if (this.player.getCurrentQuality() === -1) {
        ret = 'auto'
      }
    }
    return ret
  },

  /** Override to return title */
  getTitle: function () {
    return this.title
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    var ret = false
    if (typeof this.isLive === 'boolean') {
      ret = this.isLive
    } else if (this.getDuration() <= 0) {
      ret = true
    }
    return ret
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.player.getPlaylistItem().file
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return this.player.version
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'JWPlayer'
  },

  /** Set options using config */
  setOptions: function (e) {
    if (e && e.media) {
      this.isLive = e.media.isLive || this.isLive
      this.title = e.media.title || this.title
    }
    if (e) {
      this.isLive = e['content.isLive'] || this.isLive
      this.title = e['content.title'] || this.title
    }
    this.plugin.setOptions(e)
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Console all events if logLevel=DEBUG
    youbora.Util.logAllEvents(this.player, [
      'beforePlay', 'meta', 'playlistItem', 'setupError', 'idle', 'complete', 'buffer', 'firstFrame'
    ])

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, true)

    // Register listeners
    this.references = {
      beforePlay: this.beforeplayListener.bind(this),
      buffer: this.bufferListener.bind(this),
      play: this.playListener.bind(this),
      pause: this.pauseListener.bind(this),
      error: this.errorListener.bind(this),
      seek: this.seekingListener.bind(this),
      seeked: this.seekedListener.bind(this),
      complete: this.endedListener.bind(this),
      meta: this.metaListener.bind(this),
      playlistItem: this.playlistItemListener.bind(this),
      setupError: this.setupErrorListener.bind(this),
      idle: this.idleListener.bind(this),
      firstFrame: this.firstFrameListener.bind(this),
      time: this.timeListener.bind(this),
      adsManager: this.managerListener.bind(this),
      remove: this.removeListener.bind(this),
      playbackRateChanged: this.playrateListener.bind(this)
    }

    for (var key in this.references) {
      this.player.on(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.off(key, this.references[key])
      }
      delete this.references
    }
  },

  timeListener: function (e) {
    if (this.fatalErrored) return null
    if (e.position !== 0) this._start()
    var actualPosition = e.position
    if (!e.currentTime && !e.position) return
    if ((this.previousPosition && this.previousPosition < actualPosition) ||
      (this.getIsLive())) {
      this.fireJoin()
    }
    this.previousPosition = actualPosition
  },

  /** Listener for 'playlistItem' event. */
  playlistItemListener: function (e) {
    var playlistItem = this.player.getPlaylistItem()
    if (playlistItem && playlistItem.file.indexOf('/VideoError.mp4') > -1) {
      return null
    }
    // reset private variables
    this.duration = undefined
    this.throughput = -1
    // Set options
    this.setOptions(this.player.getConfig().youbora)
    this.setOptions(playlistItem.youbora)
    this.fireStop()
  },

  /** Listener for 'setupError' event. */
  setupErrorListener: function (e) {
    this.fireError(e.message)
  },

  /** Listener for 'idle' event. */
  idleListener: function (e) {
    if (!e.reason || e.reason === 'complete') this.fireStop()
  },

  /** Listener for 'firstFrame' event. */
  firstFrameListener: function (e) {
    this.tag = this.player.getConfig().mediaElement
    this.fireJoin()
  },

  /** Listener for 'beforePlay' event. */
  beforeplayListener: function (e) {
    // reset private variables
    this.duration = undefined
    this.throughput = -1

    // Set options from config if playlist options are not defined
    // Playlist config has higher priority, they will be set in playlistItem event
    if (!this.player.getPlaylistItem().youbora) {
      this.setOptions(this.player.getConfig().youbora)
    }

    if (!this.flags.isStarted) {
      // Send /start
      this._start()
      this.tag = this.player.getConfig().mediaElement
      this.fatalErrored = false
      // Start adnalyzer
      if (!this.plugin.getAdsAdapter()) {
        this.plugin.setAdsAdapter(new youbora.adapters.JWPlayer.GenericAdsAdapter(this.player))
      }
    }
  },

  managerListener: function (e) {
    if (!this.plugin.getAdsAdapter()) {
      this.plugin.setAdsAdapter(new youbora.adapters.JWPlayer.GenericAdsAdapter(this.player))
      this.plugin.getAdsAdapter().managerListener(e)
    }
  },

  /** Listener for 'buffer' event */
  bufferListener: function (e) {
    if (!this.flags.isPaused) {
      this.fireBufferBegin()
    }
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this.fireBufferEnd()
    this.fireSeekEnd()
    this.fireResume()
    this.monitor.skipNextTick()
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    if (this.flags.isSeeking) {
      this.fireSeekEnd()
    }
    this.firePause()
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    if (e.message && e.message.toLowerCase().includes('casting failed to load')) {
      return
    }
    if (e.code) {
      this.fireError(e.code, e.message)
    } else {
      this.fireError(e.message)
    }
    if (!e.message ||
      e.message.toLowerCase().includes('network error') ||
      e.message.toLowerCase().includes('could not be played') ||
      e.message.toLowerCase().includes('cannot load') ||
      e.message.toLowerCase().includes('cannot be played') ||
      e.message.toLowerCase().includes('down') ||
      e.message.toLowerCase().includes('fragloaderror') ||
      e.message.toLowerCase().includes('levelloadtimeout') ||
      e.message.toLowerCase().includes('file not found')) {
      this.fireStop()
    }
    if (e.code) {
      if (e.code < 300000 || e.code > 309000) {
        this.fireStop()
        this.fatalErrored = true
      }
    }
  },

  /** Listener for 'seek' event. */
  seekingListener: function (e) {
    this.fireSeekBegin()
  },

  /** Listener for 'seeked' event. */
  seekedListener: function (e) {
    if (this.player.getState() !== 'paused') {
      this.fireSeekEnd()
      this.monitor.skipNextTick()
    }
  },

  /** Listener for 'complete' event. */
  endedListener: function (e) {
    this.fireStop()
  },

  /** Listener for 'remove' event. */
  removeListener: function (e) {
    this.plugin.fireStop()
  },

  playrateListener: function (e) {
    if (this.monitor) this.monitor.skipNextTick()
  },

  _start: function(e) {
    if (!this.flags.isStarted && this.player && this.plugin) {
      this._getDrm()
      this.fireStart(e)
    }
  },

  _getDrm: function(e){
    var item = this.player.getPlaylistItem(this.player.getPlaylistIndex())
    for (var i in item.allSources) {
      var element = item.allSources[i]
      if (element.file === item.file && element.drm && typeof Object.keys === 'function'){
        var drms = Object.keys(element.drm)
        if (drms.length === 1) {
          this.plugin.setOptions({'content.drm': drms[0]})
        }
        break
      }
    }
  },

  /** Listener for 'meta' event */
  metaListener: function (e) {
    this.duration = e.duration || this.duration
    if (e.metadata) {
      this.duration = e.metadata.duration || this.duration
    }
  }
}, {
  GenericAdsAdapter: require('./ads/native')
})

module.exports = youbora.adapters.JWPlayer
