var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var GenericAdsAdapter = youbora.Adapter.extend({
  getVersion: function () {
    return manifest.version + '-jwplayer-ads'
  },

  getDuration: function () {
    return this.duration
  },

  getResource: function () {
    return this.resource
  },

  getPlayhead: function () {
    return this.playhead
  },

  getTitle: function () {
    return this.title
  },

  getPosition: function () {
    var position = youbora.Constants.AdPosition.Midroll
    if (this.plugin.getAdapter() && !this.plugin.getAdapter().flags.isJoined) {
      position = youbora.Constants.AdPosition.Preroll
    } else if (this.position === youbora.Constants.AdPosition.Preroll ||
      this.position === youbora.Constants.AdPosition.Midroll ||
      this.position === youbora.Constants.AdPosition.Postroll) {
      position = this.position
    }
    return position
  },

  getGivenBreaks: function () {
    var ret = null
    if (this.manager && this.manager.adsManager && this.manager.adsManager.getCuePoints) {
      ret = this.manager.adsManager.getCuePoints().length
    }
    return ret
  },

  getBreaksTime: function () {
    var ret = null
    if (this.manager && this.manager.adsManager && this.manager.adsManager.getCuePoints) {
      var cuelist = this.manager.adsManager.getCuePoints()
      for (var index in cuelist) {
        if ((!cuelist[index] && cuelist[index] !== 0) || cuelist[index] === -1) {
          cuelist[index] = this.plugin._adapter.getDuration()
        }
      }
      ret = cuelist
    }
    return ret
  },

  getGivenAds: function () {
    return (this.pod && this.pod.getTotalAds) ? this.pod.getTotalAds() : null
  },

  getAudioEnabled: function () {
    return !this.player.getMute()
  },

  getIsSkippable: function () {
    return this.skippable
  },

  getIsFullscreen: function () {
    if (typeof this.player.getFullscreen === 'function') {
      return this.player.getFullscreen()
    }
    var videoObject = this.player.getContainer()
    return (window.innerHeight <= videoObject.clientHeight + 30 && window.innerWidth <= videoObject.clientWidth + 30)
  },

  getIsVisible: function () {
    return youbora.Util.calculateAdViewability(this.player.getContainer())
  },

  getCreativeId: function () {
    return this.creativeId
  },

  registerListeners: function () {
    // Register listeners
    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false, 1200)

    // Register listeners
    this.references = {
      adTime: this.adTimeListener.bind(this),
      adStarted: this.adStartedListener.bind(this),
      adImpression: this.adImpressionListener.bind(this),
      adPause: this.adPauseListener.bind(this),
      adPlay: this.adPlayListener.bind(this),
      adSkipped: this.adSkippedListener.bind(this),
      adComplete: this.adCompleteListener.bind(this),
      adClick: this.adClickListener.bind(this),
      adError: this.adErrorListener.bind(this),
      adsManager: this.managerListener.bind(this)
    }

    for (var key in this.references) {
      this.player.on(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.off(key, this.references[key])
      }
      delete this.references
    }
  },

  managerListener: function (e) {
    this.manager = e
  },

  adTimeListener: function (e) {
    this.playhead = e.position
    this.duration = e.duration
    if (this.plugin._adapter) this.plugin._adapter.firePause()
    if (e.ima && e.ima.ad) this._imaMetadata(e.ima.ad)
    if (!this.firstStarted) {
      this.fireStart()
      this.firstStarted = true
    }
    this.fireJoin()
    this._quartileHandler()
  },

  _imaMetadata: function (ad) {
    this.creativeId = ad.getCreativeId() || ad.getCreativeAdId()
    this.resource = ad.getMediaUrl()
    this.clickThrough = ad.g ? ad.g.clickThroughUrl : null
    this.pod = ad.getAdPodInfo()
    this.skippable = !((ad.getSkipTimeOffset() <= 0))
  },

  _quartileHandler: function () {
    var time = this.playhead / this.duration
    if (time > 0.75) {
      this.fireQuartile(3)
    } else if (time > 0.5) {
      this.fireQuartile(2)
    } else if (time > 0.25) {
      this.fireQuartile(1)
    }
  },

  adStartedListener: function (e) {
    this.position = e.adposition
    this.resource = e.mediafile && e.mediafile.file ? e.mediafile.file : e.tag
    this.title = e.adtitle
  },

  adImpressionListener: function (e) {
    this.position = e.adposition
    this.resource = e.mediafile && e.mediafile.file ? e.mediafile.file : e.tag
    this.title = e.adtitle || e.tag
    this.clickThrough = e.clickThroughUrl
  },

  adPauseListener: function (e) {
    this.firePause()
  },

  adPlayListener: function (e) {
    this.fireResume()
  },

  adSkippedListener: function (e) {
    this.fireSkip()
    this.firstStarted = false
  },

  adCompleteListener: function (e) {
    this.fireStop()
    this.firstStarted = false
    this.resetValues()
  },

  adClickListener: function (e) {
    this.fireClick(this.clickThrough)
  },

  adErrorListener: function (e) {
    if (e.message) {
      var msg = e.message.toLowerCase()
        if (msg.indexOf('ad tag empty') > -1) {
          this.fireManifest(youbora.Constants.ManifestError.EMPTY, e.message)
        } else if (msg.indexOf('error loading ad tag') > -1 || msg.indexOf('invalid ad tag') > -1 || msg.indexOf('any valid ads') > -1) {
          this.fireManifest(youbora.Constants.ManifestError.WRONG, e.message)
        } else {
          if (e.adErrorCode && (e.adErrorCode === 60006 || e.adErrorCode === 10403)) {
            this.fireManifest(youbora.Constants.ManifestError.WRONG, e.message)
          } else {
            this.fireError(e.adErrorCode, e.message)
            this.fireStop()
            this.resetValues()
            if (msg.indexOf('timeout') > -1){
              this.fireBreakStop()
            }
          }
        }
    }
  },

  resetValues: function () {
    this.playhead = undefined
    this.duration = undefined
    this.position = undefined
    this.resource = undefined
    this.title = undefined
  }

})

module.exports = GenericAdsAdapter
